package cl.duoc.listandodatos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListarDatos extends AppCompatActivity {

    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_datos);

        lv= (ListView) findViewById(R.id.lvListar);

        String[] datos2=new String[50];

        for (int i=0; i<50; i++)
        {
            datos2[i]="Datos N° "+(i+1);
        }

        final String[] datos =
                new String[]{"Elem1","Elem2","Elem3","Elem4","Elem5"};

        ArrayAdapter<String> adaptador =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, datos2);

        lv.setAdapter(adaptador);
    }


}
